package instagramrodrunner;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class Application {
    public static void main( String[] args )
    {
        System.out.println( "Instagramrod Runner..." );

        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/970.376.8237?download=true";
        ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);

        System.out.println( "Runner Complete!" );
        System.out.println( response.getBody() );
    }
}
